<?
IncludeModuleLangFile(__FILE__);
class MlifeSiteFoto1 {
	
	function ShowPanel()
	{
		if ($GLOBALS["USER"]->IsAdmin() && COption::GetOptionString("main", "wizard_solution", "", SITE_ID) == "foto1")
		{
			$GLOBALS["APPLICATION"]->AddPanelButton(array(
				"HREF" => "/bitrix/admin/wizard_install.php?lang=".LANGUAGE_ID."&wizardName=mlife:foto1&wizardSiteID=".SITE_ID."&".bitrix_sessid_get(),
				"ID" => "foto_wizard",
				//"SRC" => "/bitrix/images/fileman/panel/web_form.gif",
				"ICON" => "bx-panel-site-wizard-icon",
				"MAIN_SORT" => 2500,
				"TYPE" => "BIG",
				"SORT" => 10,	
				"ALT" => GetMessage("MLIFE_BUTTON_DESCRIPTION"),
				"TEXT" => GetMessage("MLIFE_BUTTON_NAME"),
				"MENU" => array(),
			));
		}

	}
}
?>