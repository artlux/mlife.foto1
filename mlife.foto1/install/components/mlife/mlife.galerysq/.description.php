 <?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MLIFE_GALERYSQ_DESC_NAME"),
	"DESCRIPTION" => GetMessage("MLIFE_GALERYSQ_DESC_DESCRIPTION"),
	"ICON" => "/images/sq.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => GetMessage("MLIFE"),
		"CHILD" => array(
			"ID" => 'galery',
			"NAME" => GetMessage("MLIFE_GALERY"),
			"SORT" => 10,
		),
	),
);
?>