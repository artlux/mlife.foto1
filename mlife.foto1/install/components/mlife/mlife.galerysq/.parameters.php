<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
	$arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arProperty_LNS = array();
$rsProp = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arCurrentValues["IBLOCK_ID"]));
while ($arr=$rsProp->Fetch())
{
	$arProperty[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S")))
	{
		$arProperty_LNS[$arr["CODE"]] = "[".$arr["CODE"]."] ".$arr["NAME"];
	}
}

$arComponentParameters = array(
	"GROUPS" => array(
	"AJAX" => array(
            "NAME" => GetMessage("GALERYSQ_AJAX_TITLE"),
            "SORT" => "310",
            ),
	"SCRIPT" => array(
            "NAME" => GetMessage("GALERYSQ_SCRIPT_TITLE"),
            "SORT" => "311",
            ),
	),
);


		$arComponentParameters["PARAMETERS"]["PAGE_ELEMENT_WIDTH"] = array(
			"PARENT" => "VISUAL",
			"NAME" => GetMessage("GALERYSQ_PAGE_ELEMENT_WIDTH"),
			"TYPE" => "STRING",
			"DEFAULT" => "200",
		);
		$arComponentParameters["PARAMETERS"]["AJAX_N"] = array(
			'NAME' => GetMessage("GALERYSQ_PARAM_AJAX"),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'1' => GetMessage("GALERYSQ_PARAM_YES"),
				'0' => GetMessage("GALERYSQ_PARAM_NO"),
			),
			'DEFAULT' => '1',
			"PARENT" => "AJAX",
			"REFRESH" => "Y",
		);
		if($arCurrentValues["AJAX_N"]==1){
			$arComponentParameters["PARAMETERS"]["AJAX_N_LOAD"] = array(
				'NAME' => GetMessage("GALERYSQ_AJAX_N_LOAD"),
				'TYPE' => 'LIST',
				'VALUES' => array(
					'load.gif' => GetMessage("GALERYSQ_AJAX_N_LOAD_1"),
					'load2.gif' => GetMessage("GALERYSQ_AJAX_N_LOAD_2"),
					'load3.gif' => GetMessage("GALERYSQ_AJAX_N_LOAD_3"),
				),
				'DEFAULT' => 'load.gif',
				"PARENT" => "AJAX",
			);
		}
		$arComponentParameters["PARAMETERS"]["JQUERY"] = array(
			'NAME' => GetMessage("GALERYSQ_PARAM_JGUERY"),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'1' => GetMessage("GALERYSQ_PARAM_YES"),
				'0' => GetMessage("GALERYSQ_PARAM_NO"),
			),
			'DEFAULT' => '1',
			"PARENT" => "SCRIPT",
		);
		$arComponentParameters["PARAMETERS"]["FANCY"] = array(
			'NAME' => GetMessage("GALERYSQ_PARAM_FANSY"),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'1' => GetMessage("GALERYSQ_PARAM_YES"),
				'0' => GetMessage("GALERYSQ_PARAM_NO"),
			),
			'DEFAULT' => '1',
			"PARENT" => "SCRIPT",
		);
		$arComponentParameters["PARAMETERS"]["FANCY_TRUMB"] = array(
			'NAME' => GetMessage("GALERYSQ_PARAM_FANSY_TRUMB"),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'1' => GetMessage("GALERYSQ_PARAM_YES"),
				'0' => GetMessage("GALERYSQ_PARAM_NO"),
			),
			'DEFAULT' => '1',
			"PARENT" => "SCRIPT",
		);
		$arComponentParameters["PARAMETERS"]["WIEV_TRUMB"] = array(
			'NAME' => GetMessage("GALERYSQ_PARAM_WIEV_TRUMB"),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'1' => GetMessage("GALERYSQ_PARAM_YES"),
				'0' => GetMessage("GALERYSQ_PARAM_NO"),
			),
			'DEFAULT' => '1',
			"PARENT" => "VISUAL",
			"REFRESH" => "Y",
		);
		if($arCurrentValues["WIEV_TRUMB"]==1){
			$arComponentParameters["PARAMETERS"]["FANCY_TRUMB_WIDTH"] = array(
				"PARENT" => "VISUAL",
				"NAME" => GetMessage("GALERYSQ_PAGE_ELEMENT_WIDTH_DET"),
				"TYPE" => "STRING",
				"DEFAULT" => "100",
			);
			$arComponentParameters["PARAMETERS"]["FANCY_TRUMB_HEIGHT"] = array(
				"PARENT" => "VISUAL",
				"NAME" => GetMessage("GALERYSQ_PAGE_ELEMENT_HEIGHT_DET"),
				"TYPE" => "STRING",
				"DEFAULT" => "100",
			);
		}
		$arComponentParameters["PARAMETERS"]["IBLOCK_TYPE"] = array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("GALERYSQ_IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlockType,
			"REFRESH" => "Y",
		);
		$arComponentParameters["PARAMETERS"]["IBLOCK_ID"] = array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("GALERYSQ_IBLOCK_IBLOCK"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "Y",
			"VALUES" => $arIBlock,
			"REFRESH" => "Y",
		);
		$arComponentParameters["PARAMETERS"]["SECTION_ID"] = array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("GALERYSQ_IBLOCK_SECTION_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["SECTION_ID"]}',
		);
		$arComponentParameters["PARAMETERS"]["SECTION_URL"] =
		CIBlockParameters::GetPathTemplateParam(
			"SECTION",
			"SECTION_URL",
			GetMessage("GALERYSQ_IBLOCK_SECTION_URL"),
			"",
			"URL_TEMPLATES"
		);
		$arComponentParameters["PARAMETERS"]["IBLOCK_URL"] =
		CIBlockParameters::GetPathTemplateParam(
			"LIST",
			"IBLOCK_URL",
			GetMessage("GALERYSQ_IBLOCK_URL"),
			"",
			"URL_TEMPLATES"
		);
		$arComponentParameters["PARAMETERS"]["BROWSER_TITLE"] = array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("GALERYSQ_BPS_BROWSER_TITLE"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		);
		$arComponentParameters["PARAMETERS"]["BROWSER_BREAD"] = array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("GALERYSQ_BPS_BROWSER_BREAD"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
		);
		$arComponentParameters["PARAMETERS"]["SET_TITLE"] = array();
		$arComponentParameters["PARAMETERS"]["CACHE_TIME"] = Array("DEFAULT"=>36000000);


?>
