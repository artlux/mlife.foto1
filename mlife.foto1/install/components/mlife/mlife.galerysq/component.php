<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
//���������
$ajax = 0;
if(!empty($_REQUEST["ajax_m"])) $ajax = intval($_REQUEST["ajax_m"]);
$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);

if ($this->StartResultCache(false, $ajax))
{

	$arResult['ajax'] = $ajax;

	//�������� ������ ��������
	$arFilter = array(
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_ACTIVE" => "Y",
	);
	CModule::IncludeModule('iblock');
	$rsSection = CIBlockSection::GetList(Array(), $arFilter, false, Array('NAME','SECTION_PAGE_URL', 'LIST_PAGE_URL')); //�������� ��� � ��� ���������
	$rsSection->SetUrlTemplates("", $arParams["SECTION_URL"], $arParams["IBLOCK_URL"]);
	$arResult["IBLOCK_URL"] = '';

	while($arr = $rsSection->GetNext()) {
		
		$arResult['CATEGORYS'][] = $arr;
		
		if(!$arResult["IBLOCK_URL"]){
		$arResult["IBLOCK_URL"] = $arr['LIST_PAGE_URL'];
		}
	}

	//�������� ������ ��������� �������
	$arFilterElements['IBLOCK_ID'] = $arParams["IBLOCK_ID"];
	if($arParams["SECTION_ID"]) {
		$arFilterElements['SECTION_ID'] = $arParams["SECTION_ID"];
	}

	//$rsElement = CIBlockElement::GetList(Array(), $arFilterElements, false, false, Array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'));
	$rsElement = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilterElements, false, false, Array('ID', 'IBLOCK_SECTION_ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'));
	while($ob = $rsElement->GetNextElement()) {
	$ar['FIELDS'] = $ob->GetFields();
	$obPrewPhoto = CFile::GetByID($ar['FIELDS']['PREVIEW_PICTURE']);
	if($obPrewPhoto) {
		$ar['PHOTO']['PREV']['SRC'] = CFile::GetPath($ar['FIELDS']['PREVIEW_PICTURE']);
		$ar['PHOTO']['PREV']['KEF'] = 1;
		if($obPrewPhoto->arResult[0]['HEIGHT']!=0)
		$ar['PHOTO']['PREV']['KEF'] = round(($obPrewPhoto->arResult[0]['WIDTH']/$obPrewPhoto->arResult[0]['HEIGHT']),2);
		
		if($ar['PHOTO']['PREV']['KEF']<0.7 || $ar['PHOTO']['PREV']['KEF']==0 ) { //0.65 - ���������, ����������� ������ ����������, ���� ������ �����������, ���� ������ �������������
		$ar['PHOTO']['PREV']['ORIENTATION'] = 2;
		}
		else{
		$ar['PHOTO']['PREV']['ORIENTATION'] = 1;
		}
		
		$ar['PHOTO']['MAIN']['SRC'] = CFile::GetPath($ar['FIELDS']['DETAIL_PICTURE']);
		$arResultTemp['ITEMS'][] = $ar;
	}
	unset($obPrewPhoto);
	}
	
	//���������� ���� � ������ ����������
	$temp = false;
	$keycache = false;
	if(is_array($arResultTemp['ITEMS']) && count($arResultTemp['ITEMS'])>0) {
		foreach($arResultTemp['ITEMS'] as $key=>$items) {

			if($items['PHOTO']['PREV']['ORIENTATION']==1 and !$temp) {
			$temp[$key] = $items;
			$keycache = $key;
			}
			else if($items['PHOTO']['PREV']['ORIENTATION']==1 and $temp) {
			$arResult['ITEMS'][] = $temp[$keycache];
			$arResult['ITEMS'][] = $items;
			unset($temp);
			$temp = false;
			}
			elseif($items['PHOTO']['PREV']['ORIENTATION']==2) {
			$arResult['ITEMS'][] = $items;
			}

		}
	}

	//���� ����� �������� ���� �� ��������� ���������� ��������� ���
	if($temp) {
	$arResult['ITEMS'][] = $temp[$keycache];
	unset($temp);
	unset($keycache);
	}
	
	//�������� ������
	if($arParams["SECTION_ID"]){
		foreach($arResult['CATEGORYS'] as $value) {
			if($value['ID']==$arParams["SECTION_ID"]) {
				$arResult['CATEGORYS_ACTIVE']['NAME'] = $value['NAME'];
				$arResult['CATEGORYS_ACTIVE']['URL'] = $value['SECTION_PAGE_URL'];
				break;
			}
		}
	}else{
	$arResult['CATEGORYS_ACTIVE'] = array();
	}
	
	$this->IncludeComponentTemplate();
}

//������
if($arParams["SECTION_ID"]){
	$APPLICATION->AddChainItem($arResult['CATEGORYS_ACTIVE']['NAME'], $arResult['CATEGORYS_ACTIVE']['URL']);
}

//��������� ��������
if($arParams["SET_TITLE"]=='Y' && $arResult['CATEGORYS_ACTIVE']['NAME']){
	$APPLICATION->SetTitle($arResult['CATEGORYS_ACTIVE']['NAME']);
}

//��������� ���� ��������
if($arParams["BROWSER_TITLE"]=='Y' && $arResult['CATEGORYS_ACTIVE']['NAME']){
	$APPLICATION->SetPageProperty('title', $arResult['CATEGORYS_ACTIVE']['NAME']);
}

//��������� ���� ��������
if($arParams["BROWSER_BREAD"]=='Y' && $arResult['CATEGORYS_ACTIVE']['NAME']){
	$APPLICATION->SetPageProperty('title', $arResult['CATEGORYS_ACTIVE']['NAME']);
}

?>