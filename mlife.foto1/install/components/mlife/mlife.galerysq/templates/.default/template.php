<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
?>
<script type="text/javascript">
var $j = jQuery.noConflict();
$j(document).ready(function(){

		$j(".fancybox-thumb").fancybox({
			prevEffect	: 'none',
			nextEffect	: 'none',
			'padding' : 0,
			<?if($arParams['WIEV_TRUMB']==1){?>
			helpers	: {
				title	: {
					type: 'outside'
				},
				 thumbs	: {
					 width	: <?=$arParams['FANCY_TRUMB_WIDTH']?>,
					 height	: <?=$arParams['FANCY_TRUMB_HEIGHT']?>
				 }
			}
			<?}?>
		});
	
	$j(document).on('click','.mlifesqajax',function(event){
				event.preventDefault();
				linkLocation = $j(this).attr("href");
				$j.get(linkLocation, { ajax_m: 1}, function(data){
				
					var arraypreloadtext = [<?=GetMessage("MLIFE_ARRAY_PREALOADTEXT")?>]
					
					var randur = getRandomInt(0,10);
					
					$j('#mes div span').html(arraypreloadtext[randur]);
					$j('#mes').css({'display':'block'});
					
					$j('#Galerysq').animate({
					'opacity' : '0'
					}, 1000, function() {
					
					$j('#Galerysq').html(data);
					
					$j("#ajaxer").preloader();
					
					});
					});
	});
	
});

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

$j.fn.preloader = function(options){
	var defaults = {
		             delay:200,
					 preload_parent:"a",
					 check_timer:300,
					 ondone:function(){
				$j('#Galerysq').animate({
				'opacity': '1'
				}, 1000, function() {
				$j('#mes').css({'display':'none'});
				}
				);
					 },
					 oneachload:function(image){  },
					 fadein:500 
					};
	// variables declaration and precaching images and parent container
	 var options = $j.extend(defaults, options),
	 root = $j(this) , images = root.find("img").css({"visibility":"hidden",opacity:0}) ,  timer ,  counter = 0, i=0 , checkFlag = [] , delaySum = options.delay ,
	 init = function(){
		timer = setInterval(function(){
			if(counter>=checkFlag.length)
			{
			clearInterval(timer);
			options.ondone();
			return;
			}
			for(i=0;i<images.length;i++)
			{
				if(images[i].complete==true)
				{
					if(checkFlag[i]==false)
					{
						checkFlag[i] = true;
						options.oneachload(images[i]);
						counter++;
						delaySum = delaySum + options.delay;
					}
					$j(images[i]).css("visibility","visible").delay(delaySum).animate({opacity:1},options.fadein,
					function(){ $j(this).parent().removeClass("preloader");   });
				}
			}
			},options.check_timer) 
		 } ;
	images.each(function(){
		if($j(this).parent(options.preload_parent).length==0)
		$j(this).wrap("<a class='preloader' />");
		else
		$j(this).parent().addClass("preloader");
		checkFlag[i++] = false;
		}); 
	images = $j.makeArray(images); 
	var icon = jQuery("<img />",{
		id : 'loadingicon' ,
		src : '<?=$templateFolder?>/images/<?=$arParams['AJAX_N_LOAD']?>'
		}).hide().appendTo("body");
	timer = setInterval(function(){
		if(icon[0].complete==true)
		{
			clearInterval(timer);
			init();
			icon.remove();
			return;
		}
		},100);
	}
</script>
<?
if($arResult['ajax']==1 and $arParams['AJAX_N']==1) {
$APPLICATION->RestartBuffer();
}else {
?>



<?
}
?>

<?//echo'<pre>';print_r($arResult);echo'</pre>';?>
<div id="Galerysq">
<div class="navGalerysq">
<?
$style = '';
if($arParams['AJAX_N']==1){
$style = " mlifesqajax";
}
if($arParams["SECTION_ID"]==0){
$style2 = " active";
}
echo '<p><a class="mlifesq'.$style.$style2.'" href="'.$arResult['IBLOCK_URL'].'">'.GetMessage("MLIFE_SHOW_ALL").'</a></p>';

	foreach ($arResult['CATEGORYS'] as $cat) {
	$style2 = '';
	if($arParams["SECTION_ID"]==$cat['ID']){
	$style2 = " active";
	}
	echo '<p><a class="mlifesq'.$style.$style2.'" href="'.$cat['SECTION_PAGE_URL'].'">'.$cat['NAME'].'</a></p>';
	
	}
	?>
</div>
	<div id="mlifeGalerysq">
	<?
	$i = 0;
	foreach ($arResult['ITEMS'] as $item) {
		if($item['PHOTO']['PREV']['ORIENTATION']==2) {
		echo '<div class="varticalGalerysq" style="width:'.$arParams['PAGE_ELEMENT_WIDTH'].'px;height:'.round($arParams['PAGE_ELEMENT_WIDTH']*0.65*2,0).'px">
		<div class="mlifeGalerysq_wrapfoto" style="width:'.$arParams['PAGE_ELEMENT_WIDTH'].'px;height:'.round($arParams['PAGE_ELEMENT_WIDTH']*0.65*2,0).'px">
		<a class="fancybox-thumb" rel="fancybox-thumb" href="'.$item['PHOTO']['MAIN']['SRC'].'"><img src="'.$item['PHOTO']['PREV']['SRC'].'" alt="'.$item['FIELDS']['NAME'].'"/></a>
		</div></div>';
		}
		else{
		$i++;
			if($i==1) {
				echo '<div class="horizontalGalerysq" style="width:'.$arParams['PAGE_ELEMENT_WIDTH'].'px;height:'.round($arParams['PAGE_ELEMENT_WIDTH']*0.65*2,0).'px">';
			}
		echo '<div class="mlifeGalerysq_wrapfoto" style="width:'.$arParams['PAGE_ELEMENT_WIDTH'].'px;height:'.round($arParams['PAGE_ELEMENT_WIDTH']*0.65,0).'px">
		<a class="fancybox-thumb" rel="fancybox-thumb" href="'.$item['PHOTO']['MAIN']['SRC'].'"><img src="'.$item['PHOTO']['PREV']['SRC'].'" alt="'.$item['FIELDS']['NAME'].'"/></a></div>';
			if($i==2) {
			$i=0;
				echo '</div>';
			}
		}
	}
	if($i==1) echo '</div>';
	?>
	</div>
</div>
<?if($arResult['ajax']!=1 and $arParams['AJAX_N']==1){?>
<div id="mes"><div><img src="<?=$templateFolder?>/images/<?=$arParams['AJAX_N_LOAD']?>" alt="Prealoader"/><br/><span></span></div></div>
<?}?>
<?if($arResult['ajax']==1 and $arParams['AJAX_N']==1)
   die();?>