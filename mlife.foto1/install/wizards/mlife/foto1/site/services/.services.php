<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"settings.php",
			"menu.php", // Install menu
			"template.php", // Install template
			"theme.php", // Install theme
			"files.php", // Copy bitrix files
		),
	),

	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK"),
		"STAGES" => Array(
			"types.php", //IBlock types
			"blog.php",
			"galery.php",
			"services.php",
		),
	),

);
?>