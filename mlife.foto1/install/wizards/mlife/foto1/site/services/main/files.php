<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;


if (WIZARD_INSTALL_DEMO_DATA)
{
	$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/"); 
	
	$handle = @opendir($path);
	if ($handle)
	{
		while ($file = readdir($handle))
		{
			if (in_array($file, array(".", "..")))
				continue; 
				
			CopyDirFiles(
				$path.$file,
				WIZARD_SITE_PATH."/".$file,
				$rewrite = true, 
				$recursive = true,
				$delete_after_copy = false
			);
		}
		CModule::IncludeModule("search");
		CSearch::ReIndexAll(false, 0, Array(WIZARD_SITE_ID, WIZARD_SITE_DIR));
	}

	WizardServices::PatchHtaccess(WIZARD_SITE_PATH);

	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH, Array("SITE_DIR" => WIZARD_SITE_DIR));
	
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/contacts.php", Array("MLIFE_PHONE" => $wizard->GetVar("siteTel")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/_index.php", Array("MLIFE_SITETITLE" => $wizard->GetVar("siteName")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/contacts.php", Array("MLIFE_SKYPE" => $wizard->GetVar("siteSkype")));
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/contacts.php", Array("MLIFE_EMAIL" => $wizard->GetVar("siteEmail")));

	$arUrlRewrite = array();
	if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
	{
		include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
	}

	$arNewUrlRewrite = array(
	array(
		"CONDITION"	=>	"#^".WIZARD_SITE_DIR."blog/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	WIZARD_SITE_DIR."blog/index.php",
	),
	array(
		"CONDITION"	=>	"#^".WIZARD_SITE_DIR."services/#",
		"RULE"	=>	"",
		"ID"	=>	"bitrix:news",
		"PATH"	=>	WIZARD_SITE_DIR."services/index.php",
	),
	);
	
	foreach ($arNewUrlRewrite as $arUrl)
	{
		if (!in_array($arUrl, $arUrlRewrite))
		{
			CUrlRewriter::Add($arUrl);
		}
	}
}

$wizard =& $this->GetWizard();

?>