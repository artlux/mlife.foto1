<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

	CModule::IncludeModule('fileman');
	$arMenuTypes = GetMenuTypes(WIZARD_SITE_ID);
	if($arMenuTypes['mainmenu'] && $arMenuTypes['mainmenu'] == GetMessage("WIZ_MENU_MAINMENU_DEFAULT"))
		$arMenuTypes['mainmenu'] =  GetMessage("WIZ_MENU_MAINMENU");
		
	SetMenuTypes($arMenuTypes, WIZARD_SITE_ID);
	COption::SetOptionInt("fileman", "num_menu_param", 2, false ,WIZARD_SITE_ID);

?>