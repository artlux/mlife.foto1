<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

COption::SetOptionString("fileman", "propstypes", serialize(array(
"description"=>GetMessage("MAIN_OPT_DESCRIPTION"),
"keywords"=>GetMessage("MAIN_OPT_KEYWORDS"),
"title"=>GetMessage("MAIN_OPT_TITLE"),
)), false, $siteID);
COption::SetOptionString("iblock", "use_htmledit", "Y");
?>