<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="blog_detail">
	<div class="date"><?echo $arResult["DISPLAY_ACTIVE_FROM"]?></div>
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="imager"><img class="detail_picture" border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>"  title="<?=$arResult["NAME"]?>" /></div>
	<?endif?>
	<div class="textor"><?echo $arResult["DETAIL_TEXT"];?></div>
</div>