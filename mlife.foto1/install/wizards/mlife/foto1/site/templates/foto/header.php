<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
   global $APPLICATION;
   $pagepart = $APPLICATION->GetCurPage();
   $pagepart = str_replace(SITE_DIR,'',$pagepart);
   if($pagepart=='index.php') $pagepart = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<?$APPLICATION->ShowHead()?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.1.9.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.jscrollpane.min.js");?>
<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.jscrollpane.css");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/components/mlife/mlife.galerysq/galery/js/jquery.mousewheel-3.0.6.pack.js");?>
<title><?$APPLICATION->ShowTitle()?></title>
<script type="text/javascript">
var $jt = jQuery.noConflict();
	$jt(window).ready( function(){
		getsize();
	});
	function getsize() {
		var heightblock = $jt('#site').height() /*- $jt('#panel').height();*/ //������ ������� ������� �����
		var widblock = $jt('#site').width(); //������ ������� ����� ������� �������
		if(heightblock<900) heightblock=900;
		var heightblockpart = heightblock/3; //������ � ������ ���������� �����
		
		var widthblock = (heightblockpart*7)+0; //����� ������ ���� + ����
		
		$jt('#site .wp').css({'width': widthblock+'px', 'margin-left': '-'+(widthblock - widblock)/2+'px'});
		$jt('.wrapcolleft, .wrapcolright').css({'width': (heightblockpart*2)+'px'});
		$jt('.wrapcolleft .bg1, .wrapcolright .bg1, .mainContur .bg1').css({'width': (heightblockpart)-2+'px'});
		$jt('.wrapcolleft .bg1, .wrapcolright .bg1, .mainContur .bg1').css({'height': (heightblockpart)-2+'px'});
		$jt('.mainContur').css({'width': ((heightblockpart*3))+'px'});
		$jt('#site').css({'height': heightblock+'px'});
		$jt('.wrapvn').css({'width':heightblockpart});
		$jt('.wrapvnright').css({'width':((heightblockpart*2)-3)+'px'});
		$jt('.wrapvnright').css({'height':((heightblockpart*3)-2)+'px'});
		
		$jt('ul.left-menu').css({'height':heightblockpart});
		$jt('ul.left-menu li a').css({'line-height': $jt("ul.left-menu li").height()+'px'});
		
		$jt('.scroll-pane').jScrollPane({
		autoReinitialise: true,
		});
	};

</script>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<div id="site"><div class="wp">
<div class="wrapcolleft">
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft1.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft2.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft3.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft4.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft5.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
<div class="bg1"><?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/bgleft6.php"),
			Array(),
			Array("MODE"=>"html")
			);?></div>
</div>
<div class="mainContur">
<?if($pagepart==''){?>
		<div class="bg1">
			<div class="logo">
			<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/logo.php"),
			Array(),
			Array("MODE"=>"html")
			);?>
			</div>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto1.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto2.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto3.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
			<div class="menu">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "mainmenu", array(
	"ROOT_MENU_TYPE" => "mainmenu",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?> 
			</div>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto4.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto5.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/mainfoto6.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
		<div class="bg1">
		<?$APPLICATION->IncludeFile(
			$APPLICATION->GetTemplatePath("include_areas/foot.php"),
			Array(),
			Array("MODE"=>"html")
		);?>
		</div>
<?}else{?>
		<div class="wrapvn">
			<div class="bg1">
				<div class="logo">
				<?$APPLICATION->IncludeFile(
				$APPLICATION->GetTemplatePath("include_areas/logo.php"),
				Array(),
				Array("MODE"=>"html")
				);?>
				</div>
			</div>
			<div class="bg1">
			<div class="menu">
				<?
				$APPLICATION->IncludeComponent("bitrix:menu", "mainmenu", array(
				"ROOT_MENU_TYPE" => "mainmenu",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "3",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
				),
				false
				);
				?> 
				</div>
			</div>
			<div class="bg1">
			<?$APPLICATION->IncludeFile(
				$APPLICATION->GetTemplatePath("include_areas/foot.php"),
				Array(),
				Array("MODE"=>"html")
			);?>
			</div>
		</div>
		<div class="wrapvnright scroll-pane">
		<div class="mlifecont">
		<h1><?$APPLICATION->ShowTitle(false)?></h1>
		<div class="text">
<?}?>