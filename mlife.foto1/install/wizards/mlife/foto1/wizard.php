<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
	function InitStep()
	{
		parent::InitStep();

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "foto1";
	}
}


class SelectTemplateStep extends CSelectTemplateWizardStep
{
}

class SelectThemeStep extends CSelectThemeWizardStep
{

}

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "foto1";
		parent::InitStep();

		$templateID = $wizard->GetVar("templateID");
		$themeID = $wizard->GetVar($templateID."_themeID");

		//$siteLogo = $this->GetFileContentImgSrc(WIZARD_SITE_PATH."include/company_name.php", "/bitrix/wizards/mlife/foto1/site/templates/foto1/themes/".$themeID."/lang/".LANGUAGE_ID."/logo.gif");
		//if (!file_exists(WIZARD_SITE_PATH."include/logo.gif"))
		//	$siteLogo = "/bitrix/wizards/mlife/foto1/site/templates/foto1/themes/".$themeID."/lang/".LANGUAGE_ID."/logo.gif";
			
		//$siteBanner = $this->GetFileContentImgSrc(WIZARD_SITE_PATH."include/banner.php", "/bitrix/wizards/mlife/foto1/site/templates/foto1/images/banner.png");
		
		$wizard->SetDefaultVars(
			Array(
				"siteName" => GetMessage("WIZ_SITENAME_TEXT_DEFAULT"),
				"siteTel" => GetMessage("WIZ_SITETEL_TEXT_DEFAULT"),
				"siteSkype" => GetMessage("WIZ_SKYPE_TEXT_DEFAULT"),
				"siteEmail" => GetMessage("WIZ_EMAIL_TEXT_DEFAULT"),
					"siteVkontakte" => GetMessage("WIZ_VKONTAKTE_TEXT_DEFAULT"),
					"siteOdn" => GetMessage("WIZ_ODN_TEXT_DEFAULT"),
					"siteFacebook" => GetMessage("WIZ_FACEBOOK_TEXT_DEFAULT"),
					"siteTwitter" => GetMessage("WIZ_TWITTER_TEXT_DEFAULT"),
				"siteMetaDescription" => GetMessage("wiz_site_desc"),
				"siteMetaKeywords" => GetMessage("wiz_keywords"),
			)
		);
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
				
		$siteLogo = $wizard->GetVar("siteLogo", true);

		$this->content .= '<table width="100%" cellspacing="0" cellpadding="0">';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-name" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_SITENAME_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteName", Array("id" => "site-name", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';

		//contacts
		$this->content .= '<tr><td style="padding:10px;text-align:center;text-transform:uppercase;font-weight:bold;color:blue;">'.GetMessage("WIZ_BREAK_KONTAKT").'</tr></td>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-tel" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_SITETEL_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteTel", Array("id" => "site-tel", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';

		$this->content .= '<tr><td><br /></td></tr>';
		
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-skype" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_SKYPE_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteSkype", Array("id" => "site-skype", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-email" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_EMAIL_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteEmail", Array("id" => "site-email", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		//social
		$this->content .= '<tr><td style="padding:10px;text-align:center;text-transform:uppercase;font-weight:bold;color:blue;">'.GetMessage("WIZ_BREAK_SOCIAL").'</tr></td>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-vkontakte" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_VKONTAKTE_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteVkontakte", Array("id" => "site-vkontakte", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-odn" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_ODN_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteOdn", Array("id" => "site-odn", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-facebook" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_FACEBOOK_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteFacebook", Array("id" => "site-facebook", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		$this->content .= '<tr><td>';
		$this->content .= '<label for="site-twitter" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("WIZ_TWITTER_TEXT").'</label>';
		$this->content .= $this->ShowInputField("text", "siteTwitter", Array("id" => "site-twitter", "style" => "width:100%;font-style:italic;"));
		$this->content .= '</tr></td>';
		
		$this->content .= '<tr><td><br /></td></tr>';
		
		//other

		$firstStep = COption::GetOptionString("main", "wizard_first" . substr($wizard->GetID(), 7)  . "_" . $wizard->GetVar("siteID"), false, $wizard->GetVar("siteID")); 

		$styleMeta = 'style="display:block"';
		if($firstStep == "Y") $styleMeta = 'style="display:none"';
		
		$this->content .= '<tr><td><br /></td></tr>';
		$this->content .= '<tr><td>
		<div  id="bx_metadata" '.$styleMeta.'>
			<div class="wizard-input-form-block">
				<label for="siteMetaDescription" style="padding:10px;text-align:center;text-transform:uppercase;font-weight:bold;color:blue;display:block;width:100%;">'.GetMessage("wiz_meta_data").'</label>
				<label for="siteMetaDescription" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("wiz_meta_description").'</label>
					<div class="wizard-input-form-field wizard-input-form-field-textarea">'.
						$this->ShowInputField("textarea", "siteMetaDescription", Array("id" => "siteMetaDescription", "style" => "width:100%", "rows"=>"3")).'</div>
			</div>';
			$this->content .= '
			<div class="wizard-input-form-block">
				<label for="siteMetaKeywords" style="color:#000000;display:block;width:100%;padding-bottom:5px;font-weight:bold;">'.GetMessage("wiz_meta_keywords").'</label><br>
					<div class="wizard-input-form-field wizard-input-form-field-text">'.
						$this->ShowInputField('text', 'siteMetaKeywords', array("id" => "siteMetaKeywords", "style" => "background-color:#fff;width:99% !important")).'</div>
			</div>
		</div>';
		
		if($firstStep == "Y")
		{
			$this->content .= '<tr><td style="padding-bottom:3px;">';
			$this->content .= $this->ShowCheckboxField("installDemoData", "Y", 
				(array("id" => "install-demo-data", "onClick" => "if(this.checked == true){document.getElementById('bx_metadata').style.display='block';}else{document.getElementById('bx_metadata').style.display='none';}")));
			$this->content .= '<label for="install-demo-data">'.GetMessage("wiz_structure_data").'</label><br />';
			$this->content .= '</td></tr>';
			
			$this->content .= '<tr><td>&nbsp;</td></tr>';
		}
		else
		{
			$this->content .= $this->ShowHiddenField("installDemoData","Y");
		}
		
		$this->content .= '</table>';

		$formName = $wizard->GetFormName();
		$installCaption = $this->GetNextCaption();
		$nextCaption = GetMessage("NEXT_BUTTON");
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();
		//$res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 70, "max_width" => 190, "make_preview" => "Y"));
//		$res = $this->SaveFile("siteBanner", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 600, "max_width" => 600, "make_preview" => "Y"));
//		COption::SetOptionString("main", "wizard_site_logo", $res, "", $wizard->GetVar("siteID")); 
	}
}

class DataInstallStep extends CDataInstallWizardStep
{
	function CorrectServices(&$arServices)
	{
		$wizard =& $this->GetWizard();
		if($wizard->GetVar("installDemoData") != "Y")
		{
		}
	}
}

class FinishStep extends CFinishWizardStep
{
}
?>